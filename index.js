const mongo = require('mongodb').MongoClient
const mongodb = require('mongodb');
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const app = express();

const url = 'mongodb://localhost:27017'

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongo.connect(url, { useNewUrlParser: true } , (err, client) => {
  if (err) {
    console.error(err)
    return
  }
  console.log("Connected!, Data on http://localhost:3000/");
  const db = client.db('tantor');
  const statsCollection = db.collection('BE_test');

  const statSchema = Schema ({
    latest: {
      type: Boolean,
      required: false,
      default: false
    },
    schema_version: {
      type: Number,
      required: true,
      min: 1,
      default: false
    },
    asset_type: {
      type: String,
      required: true,
      lowercase: true
    },
    asset_id: {
      type: String,
      required: true,
      minlength: 3
    },
    stat: {
      type: String,
      required: true,
      lowercase: true
    },
    value: {
      type: Number,
      required: true
    },
    created_at: {
      type: Date,
      default: Date.now
    },
    updated_at: {
      type: Date,
      default: Date.now
    },
  });

// saving the schema
const Stat = mongoose.model("Stat", statSchema);

// GET all records
app.get('/stats', async function(req, res) {
  const stats = await statsCollection.find({}).toArray();
  res.send(stats);
});

// GET record by id
app.get('/stats/:id', async function(req, res) {
  const statId = await statsCollection.find({ _id: mongodb.ObjectID(req.params.id)}).toArray();
  res.send(statId);
});

// UPDATE record
app.patch('/stats/:id', async function(req, res) {
 const statUp = await statsCollection.updateOne(
      { _id : mongodb.ObjectID(req.params.id)},
      { $set: { value : 123 } });
 res.send(statUp);
});

// CREATE record
app.post('/stats', async function(req, res) {

  const theStat = new Stat ({
    _id: new mongoose.Types.ObjectId(),
    latest: true,
    schema_version: 1,
    asset_type: "youtube#channel",
    asset_id: "5i0Acge9m5UiRP2YsfCpxw",
    stat: "subscribers",
    value: 9999
  });

  const statCr = await statsCollection.insertOne(theStat);
  res.send(statCr);
});

// DELETE a record
app.delete('/stats/:id', async function(req, res) {
  const statDel = await statsCollection.findOneAndDelete({ _id: mongodb.ObjectID(req.params.id)})
  res.send(statDel);
});

});

const PORT = 3000;
app.listen(PORT, () => console.log(`listening on port ${PORT}`));
